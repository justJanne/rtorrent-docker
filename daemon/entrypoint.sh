#!/bin/sh
set -eu

echo > /tmp/rtorrent.log
rm /session/rtorrent.lock /session/rtorrent.pid || true

if [ -n "$EXTERNAL_IP_CMD" ]; then
  EXTERNAL_IP=$(eval "$EXTERNAL_IP_CMD")
  echo "external ip: ${EXTERNAL_IP}"
fi
if [ -n "$EXTERNAL_IP" ]; then
  rtorrent -i "$EXTERNAL_IP" -n -o "import=/rtorrent.rc" &
else
  rtorrent -n -o "import=/rtorrent.rc" &
fi

tail -f /tmp/rtorrent.log
