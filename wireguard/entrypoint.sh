#!/bin/sh
set -eu

if [ -n "$LOCAL_NETWORKS" ]; then
  gateway=$(ip route list match 0.0.0.0 | grep -v tun0 | cut -d ' ' -f 3)
  interface=$(ip route list match 0.0.0.0 | grep -v tun0 | cut -d ' ' -f 5)
  echo gateway="$gateway"
  echo interface="$interface"
fi

mkdir -p /dev/net
mknod /dev/net/tun c 10 200
chmod 0666 /dev/net/tun

wg-quick up /wireguard.conf

if [ -n "$LOCAL_NETWORKS" ]; then
  IFS=","
  for LOCAL_NETWORK in $LOCAL_NETWORKS; do
    ip route add "$LOCAL_NETWORK" via "${gateway}" dev "${interface}"
  done
fi
